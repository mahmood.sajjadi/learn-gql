const { gql } = require('apollo-server-hapi');
/*
const typeDefs = gql` 
  interface IRecord {
    _id: ID!,
  }
  interface IPage implements IRecord {
    number: Int!,
    hasNextPage: Boolean!,
    hasPreviousPage: Boolean!,
    endCursor: String,
    startCursor: String
  }
  type Page implements IPage {
    content: String!,
  }
  input CreatePage {
    bookID: ID!,
    content: String!
  }
  type Author implements IRecord {
    name: String!,
    books: [Book!]!,
  }
  input CreateAuthor {
    name: String!
  }
  type Genre implements IRecord {
    name: String!,
    books: [Book!]!,
  }
  input CreateGenre {
    name: String!
  }
  type Book implements IRecord {
    pages: [Page!]!,
    genreIDs: [ID!]!,
    genres: [Genre!]!,
    authorIDs: [ID!]!,
    authors: [Author!]!,
    year: Int!,
  }
  input CreateBook {
    pages: [Page!]!,
    genreIDs: [ID!]!,
    authorIDs: [ID!]!,
    year: Int!,
  }
  type Query {
    allAuthors: [Author!]!,
    authorsByName(name: String!): [Author!]!,
    authorByID(_id: ID): Author!,
    allGenres: [Genre!]!,
    genresByName(name: String!): [Genre!]!,
    genreByID(_id: ID): Genre!,
    allBooks: [Book!]!,
    booksByName(name: String!): [Book!]!,
    booksByYear(year: Int!): [Book!]!,
    bookByID(_id: ID): Book!,
  }
  type Mutation {
    addAuthor(author: CreateAuthor): Author,
    addGenre(genre: CreateGenre): Genre,
    addBook(genre: CreateBook): Book,
  }
`;
*/

const typeDefs = gql` 
  type Page {
    _id: ID!,
    number: Int!,
    hasNextPage: Boolean!,
    hasPreviousPage: Boolean!,
    endCursor: String,
    startCursor: String
    content: String!,
  }
  input CreatePage {
    number: Int!,
    content: String!,
  }
  type Author {
    _id: ID!,
    name: String!,
    books: [Book!]!,
  }
  input CreateAuthor {
    name: String!
  }
  type Genre {
    _id: ID!,
    name: String!,
    books: [Book!]!,
  }
  input CreateGenre {
    name: String!
  }
  type Book {
    _id: ID!,
    name: String!,
    pages: [Page!]!,
    genres: [Genre!]!,
    authors: [Author!]!,
    year: Int!,
  }
  input CreateBook {
    name: String!,
    pages: [CreatePage!]!,
    genreIDs: [ID!]!,
    authorIDs: [ID!]!,
    year: Int!,
  }
  type Query {
    allAuthors: [Author!]!,
    authorsByName(name: String!): [Author!]!,
    authorByID(_id: ID): Author!,
    allGenres: [Genre!]!,
    genresByName(name: String!): [Genre!]!,
    genreByID(_id: ID): Genre!,
    allBooks: [Book!]!,
    booksByName(name: String!): [Book!]!,
    booksByYear(year: Int!): [Book!]!,
    bookByID(_id: ID): Book!,
  }
  type Mutation {
    addAuthor(author: CreateAuthor): Author,
    addGenre(genre: CreateGenre): Genre,
    addBook(book: CreateBook): Book,
  }
`;

module.exports = typeDefs;