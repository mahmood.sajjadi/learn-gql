const {Author, Book, Genre} = require('../model');

const resolvers = {
    Query: {
        allAuthors() {
            return Author.find({});
        },
        authorsByName(name) {
            var regexp = new RegExp("^"+ name, 'i');
            return Author.find({name: regexp});
        },
        authorByID(_id) {
            return Author.findById(_id);
        },
        allGenres() {
            return Genre.find({});
        },
        genresByName(name) {
            var regexp = new RegExp("^"+ name, 'i');
            return Genre.find({name: regexp});
        },
        genreByID(_id) {
            return Genre.findById(_id);
        },
        allBooks() {
            return Book.find({});
        },
        booksByName(name) {
            var regexp = new RegExp("^"+ name, 'i');
            return Book.find({name: regexp});
        },
        booksByYear(year) {
            return Book.find({year: year});
        },
        bookByID(_id) {
            return Book.findById(_id);
        },
    },
    Mutation: {
        addAuthor(root, args) {
            const author = new Author(args.author);
            return author.save();
        },
        addGenre(root, args) {
            const genre = new Genre(args.genre);
            return genre.save();
        },
        addBook(root, args) {
            const book = new Book(args.book);
            return book.save();
        },
    }

};

module.exports = resolvers;