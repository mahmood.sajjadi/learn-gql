const mongoose = require('mongoose');

const Author = require('./authors');
const Genre = require('./genres');

const Page = { 
    number: Number,
    hasNextPage: Boolean,
    hasPreviousPage: Boolean,
    endCursor: String,
    startCursor: String,
    content: String
};

module.exports = mongoose.Schema({ 
    name: String,
    pages: [Page],
    genreIDs: [String],
    authorIDs: [String],
    year: Number,
});