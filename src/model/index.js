const mongoose = require('mongoose');

const BookSchema = require('./books');
const AuthorSchema = require('./authors');
const GenreSchema = require('./genres');

BookSchema.virtual('genres')
.get(function () {
    return Genre.find({_id: { $all : this.genreIDs }});
});

BookSchema.virtual('authors')
.get(function () {
    return Author.find({_id: { $all : this.authorIDs }});
});

AuthorSchema.virtual('books')
.get(function () {
    return Book.find({authorIDs: this._id});
});

GenreSchema.virtual('books')
.get(function () {
    return Book.find({genreIDs: this._id});
});

const Book = mongoose.model('Book', BookSchema);
const Author = mongoose.model('Author', AuthorSchema);
const Genre = mongoose.model('Genre', GenreSchema);

module.exports = {
    Book,
    Author,
    Genre,
}