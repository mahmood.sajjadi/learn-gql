const Hapi = require('hapi');
const typeDefs = require('./src/graphql/schema');
const resolvers = require('./src/graphql/resolvers');
const { ApolloServer } = require('apollo-server-hapi');

const GRAPHQL_PORT = 4000;
const MONGO_PORT = 27017;
const MONGO_URL = 'localhost';
const dbName = 'graphExample';

const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const { ObjectId } = mongoose.Types;
ObjectId.prototype.valueOf = function () {
    return this.toString();
};

const server = new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    playground: {
        settings: {
            'editor.theme': 'light',
        },
        tabs: [{
            endpoint: '/getUserById',
            query: 'getUserById',
            variables: 'id',
        },],
    },
});

const app = new Hapi.server({
    port: GRAPHQL_PORT,
    host: 'localhost'
});

async function init() {
    await server.applyMiddleware({
        app,
    });

    await server.installSubscriptionHandlers(app.listener);

    await app.start();
    console.log(`🚀 Server running at: ${app.info.uri}`);
}

mongoose.connect(`mongodb://${MONGO_URL}:${MONGO_PORT}/${dbName}`,
    { useNewUrlParser: true }
).then(_ => init().catch(error => console.log(error)));